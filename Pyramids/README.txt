This will test your interation and Looping skills

You will have to make two pyramids, that print out onto the console. Here is pyramid one:
###
Enter the number of lines: 7
      1
     212
    32123
   4321234
  543212345
 65432123456
7654321234567

###

and here is the second pyramid variation

###
Enter the number of lines: 6
                         

                        1

                    1   2   1

                1   2   4   2   1

            1   2   4   8   4   2   1

        1   2   4   8  16   8   4   2   1

    1   2   4   8  16  32  16   8   4   2   1

###

Hints:
1: to take input from the user, use something like this "levels = input('Enter the number of levels in the pyramid') "
THis will determine how high your pyramid is
2: for each line, you will have to 
	a: print out the correct amount of spaces 
	b :print out numbers (in decending or ascending order depending on the pyramid) with spacing
	c: print out numbers in the opposite order to fill the other half of the pyramid

to get the correct spacing correct, youll probably want to concatanate (add) srtings together. i.e.
	###
	spacer = " " * 3
	for i in line:
		print(spacer + str(i))
	###
this will make something like this
	###
	   0   1   2   3   4
	###

Good luck

p.s. if you want to do it more professionally, you can forgo the input command, and use the command line to enter your input using sys.argv[ x] to grab the given argument
  