make a class that reads in the data from the given file, and provides that following funtions
	-stock.print()   : returns the name, ticker, and range of availible data
	-stock.find_date("date")   :returns all the data for a given day. if the day does not exist, return false.
	-get_data() : returns all the data for that stock
	-make_data_list("data type")    : returns a list of prices of all the same type i.e. open, high, low, close
	-
	-

youll want to use json to bring in the file and os to see if a file exist
heres an example of the init function that creates the object

###
import json
from os.path import join as pjoin
import os.path

class Stock:
    def __init__(self, symbol: str , name: str="name", sector: str="sector"):
        self.__symbol = symbol.upper()
        self.__name = name
        self.__sector = sector
        filename = pjoin('C:\\Users\\Bryson M\\Documents\\Coding Projects\\USU_Invests\\stock_data',
                         self.__symbol.upper() + 'data.json')
        if os.path.isfile(filename):
                with open(filename, "r") as data:
                    self.__priceData = json.loads(data.read())
        else:
            print('file ' + symbol.upper() + 'data.json does not exist')
###

EXAMPLES
###

	